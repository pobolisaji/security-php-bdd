-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 12 août 2021 à 15:13
-- Version du serveur : 10.4.20-MariaDB
-- Version de PHP : 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bookmark`
--

-- --------------------------------------------------------

--
-- Structure de la table `catégories`
--

CREATE TABLE `catégories` (
  `id` int(11) NOT NULL,
  `nom` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `catégories`
--

INSERT INTO `catégories` (`id`, `nom`, `description`) VALUES
(1, 'Développement web', 'Job, études, apprentissage'),
(2, 'Voyages', 'Blogs voyages, photos, conseils'),
(3, 'Food', 'Bonnes adresses, recettes, idées, conseils'),
(17, 'Sport', 'Clubs, compétition, actualités'),
(18, 'Médias', 'Chaînes, émissions, actualités'),
(19, 'Réseaux sociaux', 'Réseaux sociaux, blogs, forums'),
(20, 'Autre', 'Autre'),
(21, 'Mode', 'Tendances, adresse boutiques, bons plans'),
(22, 'Nature / Animaux', 'Idées rando, jardinage, animaux');

-- --------------------------------------------------------

--
-- Structure de la table `favoris`
--

CREATE TABLE `favoris` (
  `id` int(11) NOT NULL,
  `titre` varchar(200) DEFAULT NULL,
  `url` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `favoris`
--

INSERT INTO `favoris` (`id`, `titre`, `url`) VALUES
(2, 'OpenClassRoom', 'https://openclassrooms.com/fr/'),
(4, 'Restaurant L\'Emotion', 'https://www.restaurant-lemotion.fr/'),
(5, 'Auberge Lou Pinatou', 'https://www.auberge-loupinatou.fr/'),
(6, 'Guide du Routard', 'https://www.routard.com/'),
(7, 'Village Vacances VVF', 'https://www.vvf-villages.fr/'),
(152, 'Sololearn', 'https://www.sololearn.com/home'),
(165, 'JO', 'https://olympics.com/fr/'),
(166, 'La région Provence', 'https://www.provenceweb.fr/f/region1.htm'),
(167, 'Simplon', 'https://simplonline.co/login'),
(170, 'Facebook', 'https://fr-fr.facebook.com/'),
(171, 'Marmiton', 'https://www.marmiton.org/'),
(177, 'coucou', 'coucoucestmoi.fr'),
(183, 'SPA Polignac', 'https://spahteloire.wixsite.com/spa-haute-loire'),
(184, 'Morgan', 'https://www.morgandetoi.fr/'),
(185, 'Gitlab', 'https://gitlab.com/'),
(186, 'Arte', 'https://www.arte.tv/fr/');

-- --------------------------------------------------------

--
-- Structure de la table `liens_catégories_favoris`
--

CREATE TABLE `liens_catégories_favoris` (
  `id_favoris` int(11) DEFAULT NULL,
  `id_catégories` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `liens_catégories_favoris`
--

INSERT INTO `liens_catégories_favoris` (`id_favoris`, `id_catégories`, `id`) VALUES
(NULL, 1, 43),
(2, 1, 2),
(4, 3, 4),
(5, 3, 6),
(6, 2, 7),
(7, 2, 8),
(152, 1, 34),
(165, 17, 47),
(166, 2, 48),
(167, 1, 49),
(170, 19, 52),
(171, 3, 53),
(177, 20, 59),
(183, 22, 65),
(184, 21, 66),
(185, 1, 67),
(186, 18, 68);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `age` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `catégories`
--
ALTER TABLE `catégories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `favoris`
--
ALTER TABLE `favoris`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `liens_catégories_favoris`
--
ALTER TABLE `liens_catégories_favoris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_favoris` (`id_favoris`,`id_catégories`),
  ADD KEY `id_catégories` (`id_catégories`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `catégories`
--
ALTER TABLE `catégories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `favoris`
--
ALTER TABLE `favoris`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;

--
-- AUTO_INCREMENT pour la table `liens_catégories_favoris`
--
ALTER TABLE `liens_catégories_favoris`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `liens_catégories_favoris`
--
ALTER TABLE `liens_catégories_favoris`
  ADD CONSTRAINT `liens_catégories_favoris_ibfk_1` FOREIGN KEY (`id_favoris`) REFERENCES `favoris` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `liens_catégories_favoris_ibfk_2` FOREIGN KEY (`id_catégories`) REFERENCES `catégories` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
