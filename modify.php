<?php include("connexion.php")?>
   
   <?php

        if (isset($_POST['titre']) && isset($_POST['url'])) {
        $titre =  htmlspecialchars($_POST["titre"]); //creation variable que l'on rapelle ensuite dn=ans le bindparam "$titre" car le htmlspecialchars ne marche pas si on l'appelle direct
        $url = htmlspecialchars($_POST["url"]); 
        $id = htmlspecialchars($_POST["id"]); 
        $modify = $bdd->prepare('UPDATE favoris SET titre = :titre, url = :url WHERE id = :id');
        $modify->bindParam(':titre', $titre );
        $modify->bindParam(':url', $url);
        $modify->bindParam(':id', $id);
        $modify->execute();


        $liste_categorie=  htmlspecialchars($_POST["liste_categorie"]);
        $modifycat = $bdd->prepare('UPDATE liens_catégories_favoris SET id_catégories = :sid WHERE id_favoris = :id');
        $modifycat->bindParam(':id',  $id);
        $modifycat->bindParam(':sid',  $liste_categorie);
        $modifycat->execute();
        $modifycat ->closeCursor();
        }
        $reponse = $bdd->prepare("SELECT f.id , f.titre , f.url , c.nom , c.description FROM favoris as f 
        LEFT JOIN liens_catégories_favoris as lcf ON f.id = lcf.id_favoris LEFT join catégories as c ON c.id = lcf.id_catégories where f.id = :idcurrent ");
        $reponse->bindParam(':idcurrent', $_GET['id']);
        $reponse->execute();
        $resultat = $reponse->fetchall();


        $reponseformulaire = $bdd->prepare('SELECT c.id as "categorie id", c.nom as "nom categorie" FROM catégories as c ORDER BY c.nom');
        $reponseformulaire -> execute();
        $result = $reponseformulaire ->fetchall();
        //print_r($result);
    ?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <title>Modifier</title>
    </head>

    <body>

        <header>
            <h1>Ma base de données Bookmarks </h1>
            <a href="index.php"><img src="img/home.png"></a>
        </header>

        <?php foreach ($resultat as $donnees) : ?>
            <div class="bodydiv2">
                <div class="div2">
                    <form method="post" action="modify.php?id=<?php printf("%u",$_GET['id']) ?>">
                        <input type="hidden" name="id" value="<?php printf("%u",$donnees["id"])  ?>">
                        <input type="text" name="titre" value="<?php printf("%s",$donnees["titre"])  ?>">
                        <input type="text" name="url" value="<?php printf("%s",$donnees["url"])  ?>">
        <?php endforeach; ?>
                        <select name="liste_categorie" >
                            <OPTION value="">-- Liste des catégories-- </option>
                            <?php foreach ($result as $result2): ?>
                            <OPTION value="<?php printf("%u",$result2["categorie id"]) ?>"><?php printf("%s",$result2["nom categorie"]) ?></option>
                            <?php endforeach; ?> 
                        </select>        
                        <input type="submit" name="submit" value="Modifier">
                    </form>
                </div>
            </div>
    </body>

</html>